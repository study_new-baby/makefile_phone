#include"../include/phone.h"

int init(PHONE **head){
    PHONE *newNode = (PHONE *)malloc(sizeof(PHONE));

    if(newNode == NULL){
        perror("malloc");
        return -1;
    }
    newNode->next = NULL;
    if(*head == NULL){
        *head = newNode;
    }
    return 0;
}