#include"../include/phone.h"

void create_info(PHONE* newNode){

    printf("\033[0;35m创建Id: %d\033[4m", newNode->id);

    printf("\033[0;35m\n姓名: \033[4m");
    scanf("%s",newNode->name);

    printf("\033[0;35m\n电话号码: \033[4m");
    scanf("%s",newNode->phone_number);

    printf("\033[0;35m\n家庭地址: \033[4m");
    scanf("%s",newNode->home_address);

    printf("\033[0;35m\n公司号码: \033[4m");
    scanf("%s",newNode->company_number);
    return;
}

int create(PHONE *head){
    PHONE *p = head;                                    //拷贝,记录头节点

    PHONE *newNode = (PHONE *)malloc(sizeof(PHONE));      //创建新节点
    if(newNode == NULL){
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    printf("\033[1;34m请输入信息>>>>>\033[4m\n");
    srand((unsigned)time(NULL));                        //创建随机数，保存id
    newNode->id = rand() % 100 + 1;                     //id设置是1-100

    create_info(newNode);

    newNode->next = NULL;
    if(head->next != NULL){
        head = head->next;
    }
    head->next = newNode;
    
    printf("\033[0;32m\n\t\t Create Success! \033[0m\n");
    getchar();                                           //Enter重新返回电话菜单页面
    head = p;                                            //重新回到头节点
}