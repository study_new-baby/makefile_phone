#include"../include/phone.h"

//以name来删除记录
int delete(PHONE *head){
    PHONE *p = head;                                    //保存头节点
    char _name[20];   
    bool flags = false;

    printf("Please input your want delete one name:\n");        
    scanf("%s",_name);

    printf("OK, Will delete user:%s\n",_name);   
    while(p->next != NULL){
        if(strcmp(p->next->name, _name) == 0){
            flags = true;
            PHONE *ptemp = p->next;             
            p->next = ptemp->next;                        //删除指定的节点
            free(ptemp);
            break;
        }
        p = p->next;
    }
    if(!flags){
        printf("\033[0;31mNo this one %s! \033[0m\n",_name);
        return -1;
    }else{
        printf("\033[1;32mDelete Success! \033[0m\n");
    }

    head = p;                                             //重回头节点

    return 0;
}