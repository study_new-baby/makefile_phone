#ifndef __PHONE_H
#define __PHONE_H

/*
This head file build struct phone_st
make list
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<time.h>
#include<unistd.h>
#include<assert.h>


typedef struct phone_st{            //build list 
    struct phone_st *next;
    int id;
    char name[20];
    char phone_number[20];
    char home_address[30];
    char company_number[20];
}PHONE;

int init   (PHONE **head);
int create (PHONE *head);
int delete (PHONE *head);
int display(PHONE *head);
int search (PHONE *head);

PHONE* flush  (PHONE *head);
void show_item(PHONE *head);

#endif