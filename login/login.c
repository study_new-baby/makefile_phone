#include<string.h>
#include"../include/phone.h"

extern char passwd[20];            //外部调用passwd

int login_menu(){
    int login_index = 3;                      //设置登录次数
    char pass[32] = {};

    printf("\033[1;33m+*********************************************************+\033[5m\n");
    printf("\033[1;33m|                                                          |\033[5m\n");
    printf("\033[1;33m|\t\t欢迎来到Mantic的通讯录                     |\033[5m\n");
    printf("\033[1;33m|                                                          |\033[5m\n");
    printf("\033[1;33m|\t\t温馨提示: 初始密码[Mantic]                 |\033[5m\n");
    printf("\033[1;33m|\t\t关注知乎: 勤奋的小牛                       |\033[5m\n");
    printf("\033[1;33m|                                                          |\033[5m\n");
    printf("\033[1;33m+*********************************************************+\033[5m\n");

    while(login_index > 0){
        printf("\033[1;96mPlease input your password:\033[0m\n");
        scanf("%s",pass);
        if(strcmp(pass, passwd) == 0){
            return 0;
        }else{
            login_index--;
            printf("Passwd Error! The remaining number of times is: %d\n",login_index);
        }
    }
    return -1;
}