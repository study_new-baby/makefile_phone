#include<stdio.h>
#include"../include/phone.h"

int phone_menu(){
    int opt;
    int fg = 0;

    while(1){
        system("clear");
        printf("\033[1;31m+*******************电话号码管理软件*******************+\033[7m\n");
        printf("\033[1;32m|1. 添加                                               |\033[7m\n");
        printf("\033[1;32m|2. 删除                                               |\033[7m\n");
        printf("\033[1;32m|3. 查找                                               |\033[7m\n");
        printf("\033[1;32m|4. 显示所有记录                                       |\033[7m\n");
        printf("\033[1;32m|0. 退出                                               |\033[7m\n");
        printf("\033[1;31m+******************************************************+\033[7m\n");

        printf("\033[0;33mInput your choice#\033[5m\n");
        scanf("%d",&opt);
        if(opt < 0 || opt > 4){
            fflush(stdin);                  //清空输入缓冲区，放置多输入了信息
            
            printf("\033[1;31m无效选项,请重新输入: \033[0m");
            getchar();
            getchar();
            continue;
        }
        return opt;
    }
    return 0;
}