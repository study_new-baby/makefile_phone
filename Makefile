include script/Makefile

# MODULES_MAKE = $(MAKE) -C $(1)
# MODULES_CLEAN = $(MAKE) clean -C $(1)

.PHONY : all mm  clean

all: $(Target)

mm:
	@ for i in $(SUBDIRS); do $(MAKE) -C $$i || exit 1; done

# mc:
# 	@ for i in $(SUBDIRS); do $(MAKE) -C $$i clean || exit 1; done

$(Target): mm
	$(CC) $(CFALGS) -o $(Target) $(ALLOBJS) 
	@ echo $(Target) make done!

clean: 
	@ for i in $(SUBDIRS); do $(MAKE) -C $$i clean || exit 1; done
	rm -rf $(Target)
	@ echo clean done!