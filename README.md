本项目名为电话本管理系统。
功能包括简易菜单、单链表、包括增删改查等基本操作。链表插入采用尾插法。

目录结构如下所示：

├── Makefile
├── allfree
│   ├── Makefile
│   ├── allfree.c
│   └── allfree.o
├── create
│   ├── Makefile
│   ├── create.c
│   └── create.o
├── delete
│   ├── Makefile
│   ├── delete.c
│   └── delete.o
├── display
│   ├── Makefile
│   ├── display.c
│   └── display.o
├── include
│   ├── Makefile
│   └── phone.h
├── init
│   ├── Makefile
│   ├── init.c
│   └── init.o
├── login
│   ├── Makefile
│   ├── login.c
│   └── login.o
├── main
│   ├── Makefile
│   ├── main.c
│   └── main.o
├── menu
│   ├── Makefile
│   ├── menu.c
│   └── menu.o
├── output
│   └── phone
├── phone
├── scripts
│   └── Makefile
└── search
    ├── Makefile
    ├── search.c
    └── search.o
