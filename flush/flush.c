#include"../include/phone.h"

PHONE *flush(PHONE* head){

    while(head->next != NULL){
        PHONE* ptmp = head->next;
        head->next = ptmp->next;
        free(ptmp);
    }
    free(head);
    head = NULL;
    return head;
}