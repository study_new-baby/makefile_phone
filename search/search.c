#include"../include/phone.h"

int search(PHONE *head){
    PHONE *p = head;                        //保存头节点
    
    bool flags = false;
    char _name[30];

    printf("\033[0;33mPlease input you want search name:\033[4m\n");
    scanf("%s",_name);

    while(head -> next != NULL){
        if(strcmp(head->next->name, _name) == 0){
            flags = true;
            show_item(head);
            break;
        }
        head = head->next;
    }
    if(!flags){
        printf("\033[0;31mNo this one %s! \033[0m\n",_name);
        return -1;
    }

    head = p;                               //重回头节点

    return 0;
}