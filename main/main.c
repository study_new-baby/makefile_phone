#include "../include/phone.h"

char passwd[20] = "Mantic";

PHONE *phone_head;   

int menu_manage(PHONE *head){
    int opt;                //功能选项
    int stop = 0;
    while(1){
        system("clear");    //每次选完项目清空屏幕
        opt = phone_menu();
        switch(opt){
            case 1: 
                create(head);
                getchar();
                break;
            case 2:
                delete(head);
                getchar();
                getchar();
                break;
            case 3:
                search(head);
                getchar();
                getchar();
                break;
            case 4:
                display(head);
                getchar();
                getchar();
                break;
            case 0:
                printf("\t\t\tBye,See you later\n");
                flush(head);
                stop = 1;
            default:
                break;
        }
        if(stop == 1){
            return 1;
        }
    }
    return 0;
}

int main(int argc, char* argv[]){
    int ret;

    init(&phone_head);              //初始化phone
    ret = login_menu();

    if(ret == -1){
        printf("\033[1;31m输入次数已经用尽, 系统将自动退出! \033[0m\n");
        exit(EXIT_FAILURE);
    }

    printf("\033[1;32m登录成功, 欢迎进入电话本管理系统!\033[7m\n");
    getchar();
    getchar();
    if(menu_manage(phone_head) == 0){
        printf("\033[1:31m注意: 电话本导入发生意外错误!\033[0m\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}